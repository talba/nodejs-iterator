// Iterator "superclass"
const Iterator = require('./iterator.js');

// Empty Iterator
const EmptyIterator = function () {
    Iterator.call(this, []);
};
// Inherit Iterator's prototype only
EmptyIterator.prototype.__proto__ = Iterator.prototype;

// Overwrite only the necessary methods
EmptyIterator.prototype.next = function () {
    return {done: true};
};

// Export the module
module.exports = EmptyIterator;