// ArgumentError class constructor
class ArgumentError extends Error {
    constructor(message) {
        super(message);
        this.name = "ArgumentError";
    }
};


// Export module
module.exports = ArgumentError;