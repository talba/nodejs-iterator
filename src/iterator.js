// ArgumentError exception constructor
const ArgumentError = require('./argument-error.js');


// Iterator "class"
const Iterator = function(iterable) {
    this._iterable = iterable;
    this._iterator = iterable[Symbol.iterator]();
    
    // one-based approach (index has not iterated yet)
    this._index = 0;
};

// "Reset" Iterator
Iterator.prototype[Symbol.iterator] = function () {
    this._iterator = this._iterable[Symbol.iterator]();
    this._index = 0;
    
    return this;
};

// Method to get the next element
Iterator.prototype.next = function () {
    let element = this._iterator.next();
    this._index += 1;
    
    return element;
};


// Limit decorator method
Iterator.prototype.limit = function (limit) {
    // Constrain limit parameter
    if (typeof limit !== 'number' || limit < 0 || limit !== Number.parseInt(limit)) {
        throw new ArgumentError('Supply a positive integer as limit');
    }

    
    // Reset iterator
    let self = this[Symbol.iterator]();
   
    // Override the original next() method
    const originalNextMethod = self.next;
    self.next = function () {
        let element = originalNextMethod.call(this);
        if (this._index > limit) return {done: true};
        return element;
    };
   
    // Return it
    return self;
};

// Infinite decorator method
Iterator.prototype.infinite = function () {
    // Reset iterator
    let self = this[Symbol.iterator]();
    
    // Override the original next() method
    const originalNextMethod = self.next;
    self.next = function () {
        let element = originalNextMethod.call(this);
        if (element.done) {
            // Reset iterator
            self._iterator = self._iterable[Symbol.iterator]();
            element = self._iterator.next();
        }
        
        return element;
    };
    
    // Return it
    return self;
};

// Reverse Decorator method
Iterator.prototype.reverse = function () {
    // Restart iterator
    let self = this[Symbol.iterator]();
    
    // Reverse array representation before recreating iterator
    let arrayFromIterable = Array.from(self._iterable);
    self._iterable = arrayFromIterable.reverse();
    
    // Return it
    return self[Symbol.iterator]();
};

// Slice Decorator method
Iterator.prototype.slice = function (start, end) {
    // Only integer numbers will pass
    if (typeof start !== 'number' || typeof end !== 'number' || start !== Number.parseInt(start) || end !== Number.parseInt(end)) {
        throw new ArgumentError('Supply integer numbers as start and end limits');
    }
    
    // Zero won't be allowed either
    if (start === 0 || end === 0) throw new ArgumentError('Supply a non-zero integer numbers as start and end limits');
    
    
    // Reset iterator
    let self = this[Symbol.iterator]();
    
    // Slice array representation before recreating iterator
    let arrayFromIterable = Array.from(self._iterable);
    // start should be converted to zero-based
    let $start = (start > 0)? (start - 1) : start;
    if (end > 0) {
        self._iterable = arrayFromIterable.slice($start, end);
        return self[Symbol.iterator]();
    }
    // now end should be converted too
    self._iterable = (end === -1)? arrayFromIterable.slice($start) : arrayFromIterable.slice($start, end + 1)
    
    // Return it
    return self[Symbol.iterator]();
};

// Repeat Decorator method
Iterator.prototype.repeat = function (howManyTimes) {
    // Constrain repeat decorator
    if (typeof howManyTimes !== 'number' || howManyTimes < 0 || howManyTimes !== Number.parseInt(howManyTimes)) {
        throw new ArgumentError('Supply a positive integer as the number of times to repeat the sequence');
    }
    
    // Restart iterator
    let self = this[Symbol.iterator]();
    
    // Initialize repeat counter
    let repeatCount = 1;
    // Override the original next() method
    const originalNextMethod = self.next;
    self.next = function () {
        let element = originalNextMethod.call(this);
        
        // If iterator reaches its limit, it will run over and over again
        if (element.done) {
            // Restart iterator
            self._iterator = self._iterable[Symbol.iterator]();
            element = self._iterator.next();
            
            // Next loop
            repeatCount += 1;
        }
        
        // Iterate until "how many times" is reached
        if (repeatCount > howManyTimes) return {done: true};
        // Otherwise return the next element
        return element;
    };
    
    // Return it 
    return self;
};


// Export module
module.exports = Iterator;