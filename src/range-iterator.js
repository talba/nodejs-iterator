// Iterator "superclass"
const Iterator = require('./iterator.js');

// RangeIterator constructor
const RangeIterator = function (from, to, step) {
    let _from = (typeof from !== 'number')? from.charCodeAt(0) : from,
        _to = (typeof to !== 'number')? to.charCodeAt(0) : to;
        
    this._current = _from;
    this._step = step || 1;
    
    Object.defineProperties(this, {
        _first: { value: _from, enumerable: true, configurable: true },
        _last: { value: _to, enumerable: true, configurable: true },
        
        // are the arguments passed all numbers?
        _argsAreNumbers: {
            value: (_from === from) && (_to === to),
            enumerable: true,
            configurable: true
        }
    });
}
// RangeIterator "inherits" Iterator
RangeIterator.prototype.__proto__ = Iterator.prototype;


// Iterator method
RangeIterator.prototype[Symbol.iterator] = function () {
    this._current = this._first;
    return this;
};

// Next method
RangeIterator.prototype.next = function () {
    if (this._current > this._last) return {done: true};
    
    let _current = this._current;
    let $descriptor = {
        value: (this._argsAreNumbers)? _current : String.fromCharCode(_current), 
        done: false 
    };
    this._current += this._step;
    
    return $descriptor;
};


// Export the module
module.exports = RangeIterator;