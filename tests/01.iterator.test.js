const $expect = require('unexpected');
const Iterator = require('../src/iterator.js');

describe("Iterator", function () {
  describe("Wrap iterables", function () {
    // Iterate array
    it('iterates arrays', function () {
      const iterator = new Iterator([1, 2, 3]);
      
      $expect(iterator.next(), 'to equal', {value: 1, done: false});
      $expect(iterator.next(), 'to equal', {value: 2, done: false});
      $expect(iterator.next(), 'to equal', {value: 3, done: false});
      $expect(iterator.next(), 'to equal', {done: true});
    });
    
    // Iterate string
    it('iterates strings', function () {
      const iterator = new Iterator('teste');
      
      $expect(iterator.next(), 'to equal', {value: 't', done: false});
      $expect(iterator.next(), 'to equal', {value: 'e', done: false});
      $expect(iterator.next(), 'to equal', {value: 's', done: false});
      $expect(iterator.next(), 'to equal', {value: 't', done: false});
      $expect(iterator.next(), 'to equal', {value: 'e', done: false});
      $expect(iterator.next(), 'to equal', {done: true});
    });
    
    // Iterate generator
    it('iterates generator functions', function () {
      const generator = function* () { yield* [1, 2, 3] };
      const iterator = new Iterator(generator());
      
      $expect(iterator.next(), 'to equal', {value: 1, done: false});
      $expect(iterator.next(), 'to equal', {value: 2, done: false});
      $expect(iterator.next(), 'to equal', {value: 3, done: false});
      $expect(iterator.next(), 'to equal', {done: true});
    });
    
    // Iterate Iterator wrapper
    it('iterates our iterator wrapper', function () {
      const iterator = new Iterator(new Iterator([1, 2, 3]));
      
      $expect(iterator.next(), 'to equal', {value: 1, done: false});
      $expect(iterator.next(), 'to equal', {value: 2, done: false});
      $expect(iterator.next(), 'to equal', {value: 3, done: false});
      $expect(iterator.next(), 'to equal', {done: true});
    });
    
    // Iterate custom objects that implement Symbol.iterator
    it('iterates custom iterables', function () {
      let range = {from: 1, to: 5};
      range[Symbol.iterator] = function () {
        this.current = this.from;
        return this;
      };
      range.next = function () {
        if (this.current <= this.to) return {value: this.current++, done: false};
		return {done: true};
      };
      const iterator = new Iterator(range);
      
      $expect(iterator.next(), 'to equal', {value: 1, done: false});
      $expect(iterator.next(), 'to equal', {value: 2, done: false});
      $expect(iterator.next(), 'to equal', {value: 3, done: false});
      $expect(iterator.next(), 'to equal', {value: 4, done: false});
      $expect(iterator.next(), 'to equal', {value: 5, done: false});
      $expect(iterator.next(), 'to equal', {done: true});
    });
  });
  
  describe("Decorators", function () {
      // limit decorator
      it('calls limit decorator', function () {
          const iterator = new Iterator('simple test');
          const limitedIterator = iterator.limit(5);
          
          $expect(limitedIterator.next(), 'to equal', {value: 's', done: false});
          $expect(limitedIterator.next(), 'to equal', {value: 'i', done: false});
          $expect(limitedIterator.next(), 'to equal', {value: 'm', done: false});
          $expect(limitedIterator.next(), 'to equal', {value: 'p', done: false});
          $expect(limitedIterator.next(), 'to equal', {value: 'l', done: false});
          $expect(limitedIterator.next(), 'to equal', {done: true});
          
          $expect(limitedIterator, 'to be an', Iterator);
      });
      
      // infinite decorator (a.k.a. circular list)
      it('calls infinite decorator', function () {
          const iterator = new Iterator('test'); 
          const infiniteIterator = iterator.infinite();
          
          $expect(infiniteIterator.next(), 'to equal', {value: 't', done: false});
          $expect(infiniteIterator.next(), 'to equal', {value: 'e', done: false});
          $expect(infiniteIterator.next(), 'to equal', {value: 's', done: false});
          $expect(infiniteIterator.next(), 'to equal', {value: 't', done: false});
          $expect(infiniteIterator.next(), 'to equal', {value: 't', done: false});
          
          $expect(infiniteIterator, 'to be an', Iterator);
      });
      
      // reverse decorator (a.k.a. reverse list)
      it('calls reverse decorator', function () {
          const iterator = new Iterator('abcd');
          const reversedIterator = iterator.reverse();
          
          $expect(reversedIterator.next(), 'to equal', {value: 'd', done: false});
          $expect(reversedIterator.next(), 'to equal', {value: 'c', done: false});
          $expect(reversedIterator.next(), 'to equal', {value: 'b', done: false});
          $expect(reversedIterator.next(), 'to equal', {value: 'a', done: false});
          $expect(reversedIterator.next(), 'to equal', {done: true});
          
          $expect(reversedIterator, 'to be an', Iterator);
      });
      
      // slice decorator (will use one-based index approach)
      it('calls slice decorator', function () {
          const iterator = new Iterator('Hello, World!');
          const slicedIterator = iterator.slice(2, 5);
          
          $expect(slicedIterator.next(), 'to equal', {value: 'e', done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 'l', done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 'l', done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 'o', done: false});
          $expect(slicedIterator.next(), 'to equal', {done: true});
          
          $expect(slicedIterator, 'to be an', Iterator);
      });
      
      // repeat decorator (repeats all the elements)
      it('calls repeat decorator', function () {
          const iterator = new Iterator('abc');
          const repeatIterator = iterator.repeat(2);
          
          $expect(repeatIterator.next(), 'to equal', {value: 'a', done: false});
          $expect(repeatIterator.next(), 'to equal', {value: 'b', done: false});
          $expect(repeatIterator.next(), 'to equal', {value: 'c', done: false});
          $expect(repeatIterator.next(), 'to equal', {value: 'a', done: false});
          $expect(repeatIterator.next(), 'to equal', {value: 'b', done: false});
          $expect(repeatIterator.next(), 'to equal', {value: 'c', done: false});
          $expect(repeatIterator.next(), 'to equal', {done: true});     // test fails here
          
          $expect(repeatIterator, 'to be an', Iterator);
      });
  });
  
});