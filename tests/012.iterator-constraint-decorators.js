const $expect = require('unexpected');
const Iterator = require('../src/iterator.js');
const ArgumentError = require('../src/argument-error.js');

describe("Constraints", function () {
  // Limit decorator constraints
  describe('Limit decorator', function () {
      let iterator;
      beforeEach('Set up iterator', function () {
          iterator = new Iterator([1, 3, 5, 7, 9]);
      });
      
      // Non-numeric limit
      it('throws exception on non-numeric limits', function () {
          $expect(() => iterator.limit('1.1'), 'to throw', function (e) {
              $expect(e, 'to be an', ArgumentError);
              $expect(e.message, 'to be', 'Supply a positive integer as limit');
          });
      });
      
      // Negative number
      it('throws exception on negative numbers', function () {
          $expect(() => iterator.limit(-1), 'to throw', function (e) {
              $expect(e, 'to be an', ArgumentError);
              $expect(e.message, 'to be', 'Supply a positive integer as limit');
          });
      });
      
      // Floating-point limit (a.k.a. decimal number)
      it('throws exception on decimal numbers', function () {
          $expect(() => iterator.limit(1.1), 'to throw', function (e) {
              $expect(e, 'to be an', ArgumentError);
              $expect(e.message, 'to be', 'Supply a positive integer as limit');
          });
      });
  });
      
  // Repeat decorator constraints
  describe('Repeat decorator', function () {
      let iterator;
      beforeEach('Set up iterator', function () {
          iterator = new Iterator([1, 3, 5, 7, 9]);
      });
      
      // Non-numeric limit
      it('throws exception on non-numeric limits', function () {
          $expect(() => iterator.repeat('1.1'), 'to throw', function (e) {
              $expect(e, 'to be an', ArgumentError);
              $expect(e.message, 'to be', 'Supply a positive integer as the number of times to repeat the sequence');
          });
      });
      
      // Negative number
      it('throws exception on negative numbers', function () {
          $expect(() => iterator.repeat(-1), 'to throw', function (e) {
              $expect(e, 'to be an', ArgumentError);
              $expect(e.message, 'to be', 'Supply a positive integer as the number of times to repeat the sequence');
          });
      });
      
      // Floating-point limit (a.k.a. decimal number)
      it('throws exception on decimal numbers', function () {
          $expect(() => iterator.repeat(1.1), 'to throw', function (e) {
              $expect(e, 'to be an', ArgumentError);
              $expect(e.message, 'to be', 'Supply a positive integer as the number of times to repeat the sequence');
          });
      });
  });
          
  // Slice decorator constraints
  describe('Slice decorator', function () {
      let iterator;
      beforeEach('Set up iterator', function () {
          iterator = new Iterator([1, 3, 5, 7, 9]);
      });
      
      // Non-numeric arguments
      it('throws exception on non-numbers', function () {
          $expect(() => iterator.slice('1', '3'), 'to throw', function (e) {
              $expect(e, 'to be an', ArgumentError);
              $expect(e.message, 'to be', 'Supply integer numbers as start and end limits');
          });
      });
      
      // Floating-point arguments
      it('throws exception on decimal numbers', function () {
          $expect(() => iterator.slice(1.1, 3.2), 'to throw', function (e) {
              $expect(e, 'to be an', ArgumentError);
              $expect(e.message, 'to be', 'Supply integer numbers as start and end limits');
          });
      });
      
      // It will not accept the zero as argument
      it('will not accept zero as argument', function () {
          $expect(() => iterator.slice(0, 2), 'to throw', function (e) {
              $expect(e, 'to be an', ArgumentError);
              $expect(e.message, 'to be', 'Supply a non-zero integer numbers as start and end limits');
          });
      });
      
      // End limit is a negative integer
      it('accepts negative integer as end limit', function () {
          const slicedIterator = iterator.slice(1, -2);
          
          $expect(slicedIterator.next(), 'to equal', {value: 1, done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 3, done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 5, done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 7, done: false});
          $expect(slicedIterator.next(), 'to equal', {done: true});
      });
      
      // End limit is -1
      it('accepts -1 as end limit', function () {
          const slicedIterator = iterator.slice(2, -1);
          
          $expect(slicedIterator.next(), 'to equal', {value: 3, done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 5, done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 7, done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 9, done: false});
          $expect(slicedIterator.next(), 'to equal', {done: true});
      });
      
      // Accepts negative start and end indexes
      it('accepts negative limits', function () {
          const slicedIterator = iterator.slice(-3, -1);
          
          $expect(slicedIterator.next(), 'to equal', {value: 5, done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 7, done: false});
          $expect(slicedIterator.next(), 'to equal', {value: 9, done: false});
          $expect(slicedIterator.next(), 'to equal', {done: true});
      });
      
      // It gives an empty sequence
      it('gives empty sequence', function () {
          const slicedIterator = iterator.slice(-4, 1);
          
          $expect(slicedIterator.next(), 'to equal', {done: true})
      });
  });
  
});