const $expect = require('unexpected');
const Iterator = require('../src/iterator.js');
const RangeIterator = require('../src/range-iterator.js');

describe("Range Iterator", function () {
    // it should develop a range sequence, just like PHP's range() function
    it('iterates range values', function () {
        // generate numbers from 1 to 5
        const rangeIterator = new RangeIterator(1, 5);
        
        $expect(rangeIterator.next(), 'to equal', {value: 1, done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 2, done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 3, done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 4, done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 5, done: false});
        $expect(rangeIterator.next(), 'to equal', {done: true});
        
        $expect(rangeIterator, 'to be an', Iterator);
    });
    
    // with a step, it should generate values every time such number goes through
    it('iterates range values by a step number', function () {
        // generate numbers from 1 to 5 stepping by 2
        const rangeIterator = new RangeIterator(1, 5, 2);
        
        $expect(rangeIterator.next(), 'to equal', {value: 1, done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 3, done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 5, done: false});
        $expect(rangeIterator.next(), 'to equal', {done: true});
        
        $expect(rangeIterator, 'to be an', Iterator);
    });
    
    // Char iteration
    it('iterates chars', function () {
        // generate chars from 'a' to 'e'
        const rangeIterator = new RangeIterator('a', 'e');
        
        $expect(rangeIterator.next(), 'to equal', {value: 'a', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 'b', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 'c', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 'd', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 'e', done: false});
        $expect(rangeIterator.next(), 'to equal', {done: true});
    });
    
    // Capital letters
    it('iterates capital letters', function () {
        // generate chars from 'A' to 'E'
        const rangeIterator = new RangeIterator('A', 'E');
        
        $expect(rangeIterator.next(), 'to equal', {value: 'A', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 'B', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 'C', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 'D', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: 'E', done: false});
        $expect(rangeIterator.next(), 'to equal', {done: true});
    });
    
    // Algarism iteration
    it('iterates algarisms', function () {
        // generate algarisms from '0' to '5'
        const rangeIterator = new RangeIterator('0', '5');
        
        $expect(rangeIterator.next(), 'to equal', {value: '0', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: '1', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: '2', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: '3', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: '4', done: false});
        $expect(rangeIterator.next(), 'to equal', {value: '5', done: false});
        $expect(rangeIterator.next(), 'to equal', {done: true});
    });
});