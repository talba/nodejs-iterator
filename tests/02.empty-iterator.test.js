const $expect = require('unexpected');
const EmptyIterator = require('../src/empty-iterator.js');
const Iterator = require('../src/iterator.js');


describe("Empty Iterator", function () {
    let emptyIterator;
    beforeEach('Set up empty iterator', function () {
        // set up empty iterator to use within each test run
        emptyIterator = new EmptyIterator();
    });
    
    // no iteration here because it is "empty" (null object)
    it('does not iterate', function () {
        $expect(emptyIterator.next(), 'to equal', {done: true});
        $expect(emptyIterator, 'to be an', Iterator);
    });
    
    // since it is empty, it should represent an empty array
    it('gives empty array representation', function () {
        $expect(Array.from(emptyIterator), 'to be empty');
    });
});