const $expect = require('unexpected');
const Iterator = require('../src/iterator.js');

describe("Chaining", function () {
  // infinite decoration limited by a number
  it('limits infinte decorator', function () {
      const iterator = new Iterator('abc');
      const limitedInfiniteIterator = iterator.infinite().limit(5);
      
      $expect(limitedInfiniteIterator.next(), 'to equal', {value: 'a', done: false});
      $expect(limitedInfiniteIterator.next(), 'to equal', {value: 'b', done: false});
      $expect(limitedInfiniteIterator.next(), 'to equal', {value: 'c', done: false});
      $expect(limitedInfiniteIterator.next(), 'to equal', {value: 'a', done: false});
      $expect(limitedInfiniteIterator.next(), 'to equal', {value: 'b', done: false});
      $expect(limitedInfiniteIterator.next(), 'to equal', {done: true});
      
      $expect(limitedInfiniteIterator, 'to be an', Iterator);
  });
      
  // chaining the reverse decorator
  it('chains the reverse decorator', function () {
      const iterator = new Iterator('1234');
      const chainedIterator = iterator.reverse().infinite().limit(8);
      
      $expect(chainedIterator.next(), 'to equal', {value: '4', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: '3', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: '2', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: '1', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: '4', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: '3', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: '2', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: '1', done: false});
      $expect(chainedIterator.next(), 'to equal', {done: true});
      
      $expect(chainedIterator, 'to be an', Iterator);
  });
      
  // "slicing" chained decorator
  it('slices chained decorators', function () {
      const iterator = new Iterator('Hello, World!');
      const chainedIterator = iterator.slice(1, 5).reverse().limit(4);
      
      $expect(chainedIterator.next(), 'to equal', {value: 'o', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: 'l', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: 'l', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: 'e', done: false});
      $expect(chainedIterator.next(), 'to equal', {done: true});
      
      $expect(chainedIterator, 'to be an', Iterator);
  });
      
  // repeating chained decorator
  it('repeats chained decorators', function () {
      const iterator = new Iterator('abcd');
      const chainedIterator = iterator.reverse().slice(1,2).repeat(3);
      
      $expect(chainedIterator.next(), 'to equal', {value: 'd', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: 'c', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: 'd', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: 'c', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: 'd', done: false});
      $expect(chainedIterator.next(), 'to equal', {value: 'c', done: false});
      $expect(chainedIterator.next(), 'to equal', {done: true});
      
      $expect(chainedIterator, 'to be an', Iterator);
  });
  
});